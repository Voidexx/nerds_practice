'use strict';

const handleModalShow = (e) => { 
  e.preventDefault();

  console.log('HERE');
  document.querySelector('.modal')
    .classList.add('modal-visible');

    return;
}


const handleModalHide = () => { 
  document.querySelector('.modal')
    .classList.remove('modal-visible');

    return; 
}

const registerHandlers = () => {
  const modalShowButton = document.querySelector('.contacts-button');
  const modalCloseButton = document.querySelector('.modal .close');

  modalShowButton.addEventListener('click', handleModalShow);
  modalCloseButton.addEventListener('click', handleModalHide);

  return;

}

registerHandlers();