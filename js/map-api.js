ymaps.ready(init);

function init(){ 
  let map = new ymaps.Map(document.getElementById('map'), {
    center: [59.938631, 30.323055],
    zoom: 16
  });

  let position = map.getGlobalPixelCenter();
        
  let placemark = new ymaps.Placemark(map.getCenter(), {},
    {
      iconLayout: 'default#image',
      iconImageHref: 'img/map-marker.png',
      iconImageSize: [160, 140],
      iconImageOffset: [-35, -135]
    }
  );

  map.geoObjects.add(placemark);
  map.setGlobalPixelCenter([position[0] - 200, position[1]]);
}