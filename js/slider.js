'use strict';

const switchToSlide = e => {
 let slideNumber;

  document
    .querySelector('.active')
    .classList.remove('active');

  e.target.classList.add('active');

  slideNumber = e.target.dataset.slide;

  document
    .querySelector('.slide-current')
    .classList.remove('slide-current');

  document
   .querySelector(`.slide:nth-child(${slideNumber})`)
   .classList.add('slide-current');

   return;
} 

document
  .querySelectorAll('.slider-switches-button')
  .forEach(el => el.addEventListener('click', switchToSlide));

