const getRangeSliderValues  = () => {
  const [ min, max ] = document.querySelectorAll('.range-slider input[type="range"]');
  let minValue = min.value;
  let maxValue = max.value;
  const [ minField, maxField ] = document.querySelectorAll('.filters-form-range > input[type="text"]');

  if(parseInt(minValue, 10) > parseInt(maxValue)) {
    let temp = maxValue;

    maxValue = minValue;
    minValue = temp;
  }

  minField.value = minValue;
  maxField.value = maxValue;

  return;
}

window.addEventListener('load', () => {
  document
    .querySelectorAll('input[type="range"]')
    .forEach(el => {
      el.oninput = getRangeSliderValues;
      el.oninput();
    })
})